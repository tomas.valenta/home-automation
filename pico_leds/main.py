import network
import socket
import time
import p9813

from machine import Pin, PWM

pin_clk = Pin(26, Pin.OUT)
pin_data = Pin(27, Pin.OUT)

chain = p9813.P9813(pin_clk, pin_data, 100)
chain.reset()

with open("ssid.txt", "r") as ssid_file:
    ssid = ssid_file.read()

with open("password.txt", "r") as password_file:
    password = password_file.read()

wlan = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.connect(ssid, password)

while True:
    if wlan.status() < 0 or wlan.status() >= 3:
        break

    print('waiting for connection...')
    time.sleep(1)

print('connected')
status = wlan.ifconfig()
print( 'ip = ' + status[0] )

addr = socket.getaddrinfo('0.0.0.0', 80)[0][-1]

s = socket.socket()
s.bind(addr)
s.listen(1)

print('listening on', addr)

red = 0
green = 0
blue = 0

# Listen for connections
while True:
    try:
        cl, addr = s.accept()
        print('client connected from', addr)
        request = cl.recv(1024)

        request = request.decode("utf-8")

        is_error = False
        response = "{}"

        if request.startswith("POST /leds/"):
            try:
                parsed_request = request.split("\n")[0]
                parsed_request = parsed_request.replace("POST /leds/", "")
                parsed_request = parsed_request.split(" ")[0]
                parsed_request = parsed_request.split(",")

                if len(parsed_request) != 3:
                    raise ValueError

                red = min(max(0, int(parsed_request[0])), 255)
                green = min(max(0, int(parsed_request[1])), 255)
                blue = min(max(0, int(parsed_request[2])), 255)
                
                chain.fill((red, green, blue))
                chain.write()
            except (ValueError, IndexError) as exception:
                print("bad led parameters:", exception)

                is_error = True

        if not is_error and (request.startswith("GET /leds ") or request.startswith("POST /leds")):
            response = f'{{"red":{red},"green":{green},"blue":{blue}}}'
        else:
            is_error = True

        if not is_error:
            cl.send('HTTP/1.0 200 OK\r\nContent-type: application/json\r\n\r\n')
        else:
            cl.send('HTTP/1.0 400 BAD REQUEST\r\nContent-type: application/json\r\n\r\n')

        cl.send(response)
        cl.close()

    except OSError as e:
        cl.close()
        print('connection closed')
