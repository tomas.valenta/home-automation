import RPi.GPIO as GPIO
import flask
import requests

try:
    status = requests.get("http://speakers-tv-watering.lan/watering").json()["status"]
except Exception as exception:
    print(f"Error getting initial status, assuming false:", exception)
    status = False


app = flask.Flask(__name__)


@app.route("/switch", methods=["GET", "POST"])
def api_switch():
    global status

    if flask.request.method == "POST":
        status = (
            True
            if flask.request.args.get("status") == "on"
            else False
        )

        set_status(status)

    return flask.jsonify({
        "status": status
    })


@app.route("/invert", methods=["POST"])
def api_invert():
    global status

    status = not status
    set_status(status)

    return flask.jsonify({
        "status": status
    })


def set_status(status: bool) -> None:
    if status:
        requests.post("http://speakers-tv-watering.lan/watering/on")
    else:
        requests.post("http://speakers-tv-watering.lan/watering/off")


if __name__ == "__main__":
    app.run(host="0.0.0.0")
