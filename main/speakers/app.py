import RPi.GPIO as GPIO
import flask
import requests

try:
    status = requests.get("http://speakers-tv-watering.lan/speakers").json()["status"]
except Exception as exception:
    print(f"Error getting initial status, assuming false:", exception)
    status = False


app = flask.Flask(__name__)


@app.route("/switch", methods=["GET", "POST"])
def api_switch():
    global status

    if flask.request.method == "POST":
        status = (
            True
            if flask.request.args.get("status") == "on"
            else False
        )

        set_status(status)

    return flask.jsonify({
        "status": status
    })


@app.route("/volume/add", methods=["POST"])
def api_volume_up():
    if "count" in flask.request.args:
        try:
            count = int(flask.request.args["count"])
        except ValueError:
            flask.abort(400)
    else:
        count = 1

    response = requests.post(f"http://speakers-tv-watering.lan/speakers/volume/add/{count}").json()

    print(response)
    volume = response["volume"]["master"]

    return flask.jsonify({"volume": volume})


@app.route("/volume/remove", methods=["POST"])
def api_volume_down():
    if "count" in flask.request.args:
        try:
            count = int(flask.request.args["count"])
        except ValueError:
            flask.abort(400)
    else:
        count = 1

    response = requests.post(f"http://speakers-tv-watering.lan/speakers/volume/remove/{count}").json()

    volume = response["volume"]["master"]

    return flask.jsonify({"volume": volume})


@app.route("/sound-profile", methods=["POST"])
def api_sound_profile():
    if flask.request.method == "POST":
        try:
            center_volume = int(flask.request.args.get("center", 0))

            if center_volume not in range(-8, 9):
                raise ValueError
        except ValueError:
            flask.abort(400)

        try:
            woofer_volume = int(flask.request.args.get("woofer", 0))

            if woofer_volume not in range(-8, 9):
                raise ValueError
        except ValueError:
            flask.abort(400)

        try:
            rear_volume = int(flask.request.args.get("rear", 0))

            if rear_volume not in range(-8, 9):
                raise ValueError
        except ValueError:
            flask.abort(400)

        try:
            front_volume = int(flask.request.args.get("front", 0))

            if front_volume not in range(-8, 9):
                raise ValueError
        except ValueError:
            flask.abort(400)

        requests.post(f"http://speakers-tv-watering.lan/speakers/sound-profile/{center_volume},{woofer_volume},{rear_volume},{front_volume}")

    return flask.jsonify({
        "center": center_volume,
        "woofer": woofer_volume,
        "rear": rear_volume,
        "front": front_volume,
    })


@app.route("/invert", methods=["POST"])
def api_invert():
    global status

    status = not status
    set_status(status)

    return flask.jsonify({
        "status": status
    })


def set_status(status: bool) -> None:
    if status:
        requests.post("http://speakers-tv-watering.lan/speakers/on")
    else:
        requests.post("http://speakers-tv-watering.lan/speakers/off")


if __name__ == "__main__":
    app.run(host="0.0.0.0")
