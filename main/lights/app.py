import RPi.GPIO as GPIO
import time
import os
import flask


# --- BEGIN GPIO ---

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

LIGHT = 4

GPIO.setup(LIGHT, GPIO.OUT)

light_on = GPIO.LOW


def set_light_state(state=None):
    api_lock_file = open(".api.lock", "x")
    api_lock_file.close()

    global light_on

    if state is not None:
        GPIO.output(LIGHT, state)
        light_on = state

    time.sleep(0.25)

    os.remove(".api.lock")


def update_light_state():
    global light_on

    light_on = GPIO.input(LIGHT)


update_light_state()


# --- END GPIO ---


# --- BEGIN Flask ---

app = flask.Flask(__name__)


@app.route("/switch", methods=["GET", "POST"])
def api_switch():
    update_light_state()

    global light_on

    if flask.request.method == "POST":
        state = (
            GPIO.HIGH
            if flask.request.args.get("state") == "on"
            else GPIO.LOW
        )

        set_light_state(state)

    return flask.jsonify({
        "state": bool(light_on),
    })


@app.route("/invert", methods=["POST"])
def api_invert():
    update_light_state()

    global light_on

    state = not light_on

    set_light_state(state)

    return flask.jsonify({
        "state": bool(light_on)
    })


if __name__ == "__main__":
    app.run(host="0.0.0.0")

# --- END Flask ---
