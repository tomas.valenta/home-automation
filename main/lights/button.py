import RPi.GPIO as GPIO
import sys
import signal
import os
import time


BUTTON = 21
LIGHT = 4

time_elapsed = 0
triggered = False


if __name__ == '__main__':
    GPIO.setwarnings(False)

    GPIO.setmode(GPIO.BCM)

    GPIO.setup(LIGHT, GPIO.OUT)

    GPIO.setup(BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

    while True:
        if GPIO.input(BUTTON):
            time_elapsed += 1

            if time_elapsed >= 2 and not triggered:
                if GPIO.input(LIGHT):
                    GPIO.output(LIGHT, GPIO.LOW)
                else:
                    GPIO.output(LIGHT, GPIO.HIGH)

                triggered = True
        else:
            triggered = False

            time_elapsed = 0

        time.sleep(0.005)
