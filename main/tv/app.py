import RPi.GPIO as GPIO
import flask
import requests

app = flask.Flask(__name__)


@app.route("/invert", methods=["POST"])
def api_invert():
    toggle()

    return flask.jsonify({})


def toggle() -> None:
    requests.post("http://speakers-tv-watering.lan/tv/toggle")


if __name__ == "__main__":
    app.run(host="0.0.0.0")
