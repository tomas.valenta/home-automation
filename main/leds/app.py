import flask
import requests

red = 0
green = 0
blue = 0

try:
    colors = requests.get("http://leds.lan/leds").json()

    red = colors["red"]
    green = colors["green"]
    blue = colors["blue"]
except Exception as exception:
    print(f"Error getting initial status, assuming false:", exception)
    status = False


app = flask.Flask(__name__)


@app.route("/set", methods=["GET", "POST"])
def api_set():
    global red
    global green
    global blue

    if flask.request.method == "POST":
        try:
            red = int(flask.request.args.get("red", 0))

            if red not in range(0, 256):
                raise ValueError
        except ValueError:
            flask.abort(400)

        try:
            green = int(flask.request.args.get("green", 0))

            if green not in range(0, 256):
                raise ValueError
        except ValueError:
            flask.abort(400)

        try:
            blue = int(flask.request.args.get("blue", 0))

            if blue not in range(0, 256):
                raise ValueError
        except ValueError:
            flask.abort(400)

        requests.post(f"http://leds.lan/leds/{red},{green},{blue}")

    return flask.jsonify({
        "red": red,
        "green": green,
        "blue": blue,
    })


if __name__ == "__main__":
    app.run(host="0.0.0.0")
