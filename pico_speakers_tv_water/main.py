import network
import socket
import time
import utime

from machine import Pin, PWM

speakers_power_pin = Pin(21, Pin.OUT)
speakers_power_pin.value(1)

speakers_volume_add_pin = Pin(20, Pin.OUT)
speakers_volume_add_pin.value(1)

speakers_volume_remove_pin = Pin(19, Pin.OUT)
speakers_volume_remove_pin.value(1)

speakers_function_button_pin = Pin(18, Pin.OUT)
speakers_function_button_pin.value(1)

tv = Pin(28, Pin.OUT)

TV_SIGNAL_CODE = "2685 864 468 837 469 410 467 411 466 867 887 452 466 410 467 411 466 410 467 408 469 409 468 409 468 409 468 410 467 411 466 410 467 412 882 443 466 838 468 409 468 83603 2688 864 468 841 465 409 468 411 466 898 855 451 467 409 468 409 468 409 468 409 468 410 467 409 468 411 466 411 466 411 467 408 469 409 910 416 468 838 468 408 469"

TV_HIGH_SIGNAL = int((2 ** 16) / 2)
TV_LOW_SIGNAL = 0
tv_signal_is_on = True

with open("ssid.txt", "r") as ssid_file:
    ssid = ssid_file.read()

with open("password.txt", "r") as password_file:
    password = password_file.read()

wlan = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.connect(ssid, password)

max_wait = 10

while max_wait > 0:
    if wlan.status() < 0 or wlan.status() >= 3:
        break
    max_wait -= 1
    print('waiting for connection...')
    time.sleep(1)

if wlan.status() != 3:
    raise RuntimeError('network connection failed')
else:
    print('connected')
    status = wlan.ifconfig()
    print( 'ip = ' + status[0] )

addr = socket.getaddrinfo('0.0.0.0', 80)[0][-1]

s = socket.socket()
s.bind(addr)
s.listen(1)

print('listening on', addr)

def send_code(pin, code, freq=38000):
    pwm = PWM(pin)
    pwm.freq(freq)
    global tv_signal_is_on

    for time in code.split(" "):
        if tv_signal_is_on:
            pwm.duty_u16(TV_HIGH_SIGNAL)
        else:
            pwm.duty_u16(TV_LOW_SIGNAL)

        utime.sleep_us(int(time))
        tv_signal_is_on = not tv_signal_is_on

    pwm.deinit()

def add_volume():
    speakers_volume_add_pin.value(0)
    utime.sleep(0.01)
    speakers_volume_add_pin.value(1)

def remove_volume():
    speakers_volume_remove_pin.value(0)
    utime.sleep(0.01)
    speakers_volume_remove_pin.value(1)

def push_function_button():
    speakers_function_button_pin.value(0)
    utime.sleep(0.01)
    speakers_function_button_pin.value(1)

DEFAULT_MASTER_VOLUME = 45
DEFAULT_ADDITIONAL_SPEAKERS_VOLUME = 0

master_volume = 45
center_volume = 0
woofer_volume = 0
rear_volume = 0
front_volume = 0

def reset_volumes():
    global DEFAULT_MASTER_VOLUME, DEFAULT_ADDITIONAL_SPEAKERS_VOLUME
    global master_volume, center_volume, woofer_volume, rear_volume, front_volume

    master_volume = DEFAULT_MASTER_VOLUME
    center_volume = woofer_volume = rear_volume = front_volume = DEFAULT_ADDITIONAL_SPEAKERS_VOLUME

# Listen for connections
while True:
    try:
        cl, addr = s.accept()
        print('client connected from', addr)
        request = cl.recv(1024)

        request = request.decode("utf-8")
        response = "{}"
        is_error = False

        speakers_on = request.startswith("POST /speakers/on ")
        speakers_off = request.startswith("POST /speakers/off ")

        speakers_volume_add = request.startswith("POST /speakers/volume/add ")
        speakers_volume_remove = request.startswith("POST /speakers/volume/remove ")

        speakers_volume_add_counted = request.startswith("POST /speakers/volume/add/")
        speakers_volume_remove_counted = request.startswith("POST /speakers/volume/remove/")

        speakers_set_sound_profile = request.startswith("POST /speakers/sound-profile/")

        toggle_tv = request.startswith("POST /tv/toggle ")

        if speakers_on:
            print("speakers on")

            reset_volumes()  # Just in case

            speakers_power_pin.value(0)

        elif speakers_off:
            print("speakers off")

            reset_volumes()

            speakers_power_pin.value(1)

        elif speakers_volume_add:
            print("speakers volume up")
            master_volume += 1
            add_volume()

        elif speakers_volume_remove:
            print("speakers volume down")
            master_volume -= 1
            remove_volume()

        elif speakers_volume_add_counted:
            try:
                parsed_request = request.split("\n")[0]
                parsed_request = parsed_request.replace("POST /speakers/volume/add/", "")
                parsed_request = parsed_request.split(" ")[0]

                count = min(max(0, int(parsed_request[0])), 62)

                for _ in range(count):
                    if master_volume < 62:
                        add_volume()
                        master_volume += 1
                        utime.sleep(0.02)
            except (ValueError, IndexError) as exception:
                is_error = True

        elif speakers_volume_remove_counted:
            try:
                parsed_request = request.split("\n")[0]
                parsed_request = parsed_request.replace("POST /speakers/volume/remove/", "")
                parsed_request = parsed_request.split(" ")[0]

                count = min(max(0, int(parsed_request[0])), 62)

                for _ in range(count):
                    if master_volume > 0:
                        remove_volume()
                        master_volume -= 1
                        utime.sleep(0.02)
            except (ValueError, IndexError) as exception:
                is_error = True

        elif speakers_set_sound_profile:
            print("setting sound profile")

            try:
                parsed_request = request.split("\n")[0]
                parsed_request = parsed_request.replace("POST /speakers/sound-profile/", "")
                parsed_request = parsed_request.split(" ")[0]
                parsed_request = parsed_request.split(",")

                if len(parsed_request) != 4:
                    raise ValueError

                new_center_volume = min(max(-8, int(parsed_request[0])), 8)
                new_woofer_volume = min(max(-8, int(parsed_request[1])), 8)
                new_rear_volume = min(max(-8, int(parsed_request[2])), 8)
                new_front_volume = min(max(-8, int(parsed_request[2])), 8)

                # -- Center volume --

                push_function_button()
                utime.sleep(0.25)

                if new_center_volume > center_volume:
                    for _ in range(new_center_volume - center_volume):
                        add_volume()
                        utime.sleep(0.02)
                elif new_center_volume < center_volume:
                    for _ in range(center_volume - new_center_volume):
                        remove_volume()
                        utime.sleep(0.02)

                center_volume = new_center_volume

                # -- Woofer volume --

                utime.sleep(0.25)
                push_function_button()
                utime.sleep(0.25)

                if new_woofer_volume > woofer_volume:
                    for _ in range(new_woofer_volume - woofer_volume):
                        add_volume()
                        utime.sleep(0.02)
                elif new_woofer_volume < woofer_volume:
                    for _ in range(woofer_volume - new_woofer_volume):
                        remove_volume()
                        utime.sleep(0.02)

                woofer_volume = new_woofer_volume

                # -- Rear volume --

                utime.sleep(0.25)
                push_function_button()
                utime.sleep(0.25)

                if new_rear_volume > rear_volume:
                    for _ in range(new_rear_volume - rear_volume):
                        add_volume()
                        utime.sleep(0.02)
                elif new_rear_volume < rear_volume:
                    for _ in range(rear_volume - new_rear_volume):
                        remove_volume()
                        utime.sleep(0.02)

                rear_volume = new_rear_volume

                # -- Front volume --

                utime.sleep(0.25)
                push_function_button()
                utime.sleep(0.25)

                if new_front_volume > front_volume:
                    for _ in range(new_front_volume - front_volume):
                        add_volume()
                        utime.sleep(0.02)
                elif new_front_volume < front_volume:
                    for _ in range(front_volume - new_front_volume):
                        remove_volume()
                        utime.sleep(0.02)

                front_volume = new_front_volume

                utime.sleep(0.25)
                push_function_button()
            except (ValueError, IndexError) as exception:
                print("bad sound profile parameters:", exception)

                is_error = True

        elif toggle_tv:
            print("toggling TV")
            send_code(tv, TV_SIGNAL_CODE)

        if not is_error and request.startswith("POST /speakers"):
            response = f'''{{
    "status":{"true" if speakers_power_pin.value() == 0 else "false"},
    "volume": {{
        "master": {master_volume},
        "center": {center_volume},
        "woofer": {woofer_volume},
        "rear": {rear_volume},
        "front": {front_volume}
    }}
}}'''.replace("\n", "").replace(" ", "")
        else:
            is_error = True

        if not is_error:
            cl.send('HTTP/1.0 200 OK\r\nContent-type: application/json\r\n\r\n')
        else:
            cl.send('HTTP/1.0 400 BAD REQUEST\r\nContent-type: application/json\r\n\r\n')

        cl.send(response)
        cl.close()

    except OSError as e:
        cl.close()
        print('connection closed')
