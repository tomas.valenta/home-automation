import network
import socket
import time

from machine import Pin, PWM

boiler = Pin(0, Pin.OUT)
boiler.value(1)

with open("ssid.txt", "r") as ssid_file:
    ssid = ssid_file.read()

with open("password.txt", "r") as password_file:
    password = password_file.read()

wlan = network.WLAN(network.STA_IF)
wlan.active(True)
wlan.connect(ssid, password)

while True:
    if wlan.status() < 0 or wlan.status() >= 3:
        break

    print('waiting for connection...')
    time.sleep(1)

print('connected')
status = wlan.ifconfig()
print( 'ip = ' + status[0] )

addr = socket.getaddrinfo('0.0.0.0', 80)[0][-1]

s = socket.socket()
s.bind(addr)
s.listen(1)

print('listening on', addr)

# Listen for connections
while True:
    try:
        cl, addr = s.accept()
        print('client connected from', addr)
        request = cl.recv(1024)

        request = request.decode("utf-8")

        boiler_on = request.startswith("POST /boiler/on ")
        boiler_off = request.startswith("POST /boiler/off ")
        boiler_timed = request.startswith("POST /boiler/timed/")

        response = "{}"
        is_error = False

        if boiler_on:
            print("boiler on")
            boiler.value(0)
        elif boiler_off:
            print("boiler off")
            boiler.value(1)
        elif boiler_timed:
            try:
                parsed_request = request.split("\n")[0]
                parsed_request = parsed_request.replace("POST /boiler/timed/", "")
                parsed_request = parsed_request.split(" ")[0]

                wait_time = min(max(0, int(parsed_request[0])), 10)

                boiler.value(0)

                time.sleep(wait_time)

                boiler.value(1)
            except (ValueError, IndexError) as exception:
                is_error = True

            # Turn off the boiler no matter what
            boiler.value(1)

        if not is_error and request.startswith("/boiler"):
            response = f'{{"status":{"true" if boiler.value() == 0 else "false"}}}'
        else:
            is_error = True

        if not is_error:
            cl.send('HTTP/1.0 200 OK\r\nContent-type: application/json\r\n\r\n')
        else:
            cl.send('HTTP/1.0 400 BAD REQUEST\r\nContent-type: application/json\r\n\r\n')

        cl.send(response)
        cl.close()

    except OSError as e:
        cl.close()
        print('connection closed')
